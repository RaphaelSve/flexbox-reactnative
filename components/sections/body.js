import {Linking, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

export const body = () => {
  return (
    <View style={styles.Container}>
      <View style={styles.sideBarContainer}>
        <Text>
          <Icon name="address-book" size={40} color="#fff" />
          <Icon name="android" size={40} color="#fff" />
          <Icon name="angellist" size={40} color="#fff" />
        </Text>
      </View>

      <View style={styles.bodyContent}>
        <Text>Juste un peu de texte</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 10,
    flexDirection: 'row',
  },
  sideBarContainer: {
    flex: 1,
    backgroundColor: '#90be6d',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
  },

  bodyContent: {
    flex: 3,
    backgroundColor: '#e9d8a6',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default body;
