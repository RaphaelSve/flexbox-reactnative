import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

export const sidebar = () => {
  return (
    <View style={styles.sideBarContainer}>
      <Text>Bonjour</Text>
    </View>
  );
};

export default sidebar;

const styles = StyleSheet.create({
    sideBarContainer : {
        flex: 1,
        height: 85,
        backgroundColor: '#90be6d',
        flexDirection :'column',
    }

});
