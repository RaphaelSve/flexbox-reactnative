import {View, Text, StyleSheet, Button, Pressable} from 'react-native';
import React from 'react';


export const NavBar = () => {
  return (
    <View style={styles.navBarContainer}>
        <Pressable style={styles.navBarContentButton}>
          <Text style={styles.navBarContentText}>Click</Text>
        </Pressable>
        <Text style={styles.navBarContentText}>Ceci est mon APP !</Text>
        <Text style={styles.navBarContentText}>Petit texte</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  navBarContainer: {
    flex: 1,
    height: 85,
    backgroundColor: '#90be6d',
    flexDirection :'row',
    justifyContent : 'space-between',
    alignItems : 'center'
  },

  navBarContentButton: {
    alignItems: 'center',
    borderRadius : 4,
    backgroundColor : '#1d3557',
    padding : 5,
},

  navBarContentText: {
    fontSize: 20,
    fontFamily: 'Roboto',
    color: 'white',
  },
});

export default NavBar;
