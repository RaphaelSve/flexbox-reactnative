import { Text, View } from 'react-native';
import React, { Component } from 'react';
import NavBar from './components/NavBar';
import {body as Body}  from './components/sections/body';


export const App = () => {
  return (
    <View style={{flex: 1}}>
      <NavBar></NavBar>
      <Body>

      </Body>


    </View>
  )
}

export default App;